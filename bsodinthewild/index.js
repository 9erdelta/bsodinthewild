require('marko/node-require').install()
require('marko/express')

var express = require('express')
var app = express()
var path = require('path')
// Constants
var DEFAULT_PORT = 8080;
var PORT = process.env.PORT || DEFAULT_PORT;
var indexTemplate = require('./views/index.marko')

app.use(express.static('static'))

// viewed at http://localhost:8080
app.get('/', function(req, res) {
  res.marko(indexTemplate)
})

app.listen(PORT, () => {
  console.log('Server running on:\nhttp://localhost:' + PORT + '/')

  if(process.send) {
    process.send('online')
  }
})
