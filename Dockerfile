FROM nginx:1.10.2-alpine
MAINTAINER ninerdelta
COPY ./nginx.conf /etc/nginx/nginx.conf
COPY ./bsodinthewild /usr/share/nginx/html
